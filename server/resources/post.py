import os
from secrets import token_urlsafe

from flask_restful import abort, fields, marshal, marshal_with, reqparse, Resource
from werkzeug.datastructures import FileStorage
from flask import current_app, request

from server.models import db, Post

parser = reqparse.RequestParser()
parser.add_argument('description', required=True)
parser.add_argument('image', type=FileStorage, location='files',
                    required=True)

parser_page = reqparse.RequestParser()
parser_page.add_argument('page', type=int, location='args')

post_fields = {
    'id': fields.Integer,
    'description': fields.String,
    'image_url': fields.String,
    'created_at': fields.DateTime,
    'updated_at': fields.DateTime,
}

ALLOWED_EXTENSIONS = set(['jpg', 'jpeg', 'png'])


def file_extension(filename):
    """Get the file extension from filename, without the leading dot."""
    ext = os.path.splitext(filename)[1].lower()
    return ext[1:]


def allowed_file(filename):
    """Check if the file is allowed (if it's an image, in this case)."""
    return file_extension(filename) in ALLOWED_EXTENSIONS


def get_post_or_abort(id):
    """Attempt to get the post from the database and abort if not found."""
    post = Post.query.get(id)
    if not post:
        abort(404, message='The post {} was not found.'.format(id))
    return post


def bad_request(message):
    """Abort with a 400 status code and a message."""
    abort(400, message=message)


class PostList(Resource):
    def get(self):
        args = parser_page.parse_args()
        page = args['page']
        posts = Post.query.order_by(Post.created_at.desc()).paginate(
            page=page,
            per_page=current_app.config['POSTS_PER_PAGE'],
            error_out=False,
        )
        data = {
            'posts': marshal(posts.items, post_fields),
            'page': posts.page,
            'pages': posts.pages,
            'next_page': posts.next_num,
            'prev_page': posts.prev_num,
        }
        return data

    @marshal_with(post_fields)
    def post(self):
        args = parser.parse_args()
        description = args['description']
        image = args['image']

        if not description.strip() or not image:
            bad_request('You must provide a description and an image.')

        if image and allowed_file(image.filename):
            filename = token_urlsafe(6) + '.' + file_extension(image.filename)
            image.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
        else:
            bad_request('You must provide a valid image.')

        image_url = '{}static/{}'.format(request.host_url, filename)
        new_post = Post(description=description, image_url=image_url)

        # Save the new post to the database.
        db.session.add(new_post)
        db.session.commit()

        return new_post, 201

class PostItem(Resource):
    @marshal_with(post_fields)
    def get(self, id):
        return get_post_or_abort(id)

    def delete(self, id):
        post = get_post_or_abort(id)
        db.session.delete(post)
        db.session.commit()
        return '', 204

    def patch(self, id):
        _parser = parser.copy().remove_argument('image')
        args = _parser.parse_args()
        description = args['description']
        if not description.strip():
            bad_request('You must provide a description.')

        post = get_post_or_abort(id)
        post.description = description
        db.session.commit()

        return '', 204
