"""Main Flask application module for the server."""

import os

from flask import Flask
from werkzeug.utils import find_modules, import_string

from server.config import DevelopmentConfig


def create_app(config=None):
    """Configurable application factory.

    The optional argument 'config' must be an object class that will
    override or extend the default configuration.  By default, a
    configuration for development environment is used.

    Return a new configured application.
    """
    app = Flask(__name__)

    app.config.from_object(config or DevelopmentConfig)
    app.config.update({
        'UPLOAD_FOLDER': os.path.join(app.root_path, 'static')
    })
    from server.models import db
    db.init_app(app)
    register_blueprints(app)

    return app


def register_blueprints(app):
    """Register all blueprint modules.

    The blueprints are found in the server/blueprints/ directory, or
    the server.blueprints package.  Each module in this package must
    provide a bp variable, which is the blueprint to be exported and
    applied to the application.

    Reference: Armin Ronacher, "Flask for Fun and Profit" - PyBay
    2016.
    """
    for name in find_modules('server.blueprints'):
        mod = import_string(name)
        if hasattr(mod, 'bp'):
            app.register_blueprint(mod.bp)
    return None
