from flask import Blueprint
from flask_restful import Api

from server.resources.post import PostList, PostItem

bp = Blueprint('api', __name__)
api = Api(bp, prefix='/api')

# Register the resources to the application.
api.add_resource(PostList, '/posts')
api.add_resource(PostItem, '/posts/<int:id>')
