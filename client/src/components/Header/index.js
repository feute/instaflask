import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { CSSTransitionGroup } from 'react-transition-group'

import UploadDialog from './UploadDialog'

class NavbarEnd extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalActive: false
    }
  }

  handleClick = () => {
    this.setState((prevState) => ({ modalActive: !prevState.modalActive }))
  }

  render() {
    return (
      <div className="navbar-end">
        <a className="navbar-item" onClick={this.handleClick}>
          <span className="icon">
            <i className="ion-ios-cloud-upload-outline is-size-3"></i>
          </span>
        </a>
        <UploadDialog isActive={this.state.modalActive} />
      </div>
    )
  }
}

const Header = () => (
  <CSSTransitionGroup
    transitionName="navbar"
    transitionAppear={true}
    transitionAppearTimeout={100}
    transitionEnter={false}
    transitionLeave={false}
  >
    <nav className="navbar is-transparent is-fixed-top" aria-label="main navigation">
      <div className="container">
        <div className="navbar-brand">
          <Link className="navbar-item" to="/">Instaflask</Link>
        </div>
        <div className="navbar-menu">
          <NavbarEnd />
        </div>
      </div>
    </nav>
  </CSSTransitionGroup>
)

export default Header
