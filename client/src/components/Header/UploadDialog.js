import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { CSSTransitionGroup } from 'react-transition-group'
import axios from 'axios'

class UploadDialog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      active: props.isActive,
      fileDescription: '',
      isSubmitting: false,
      image: null,
      description: '',
      error: '',
      redirect: false,
      newPostId: 1,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isActive !== this.props.isActive) {
      this.toggleActive(null)
    }
  }

  toggleActive = (e) => {
    if (e) {
      e.preventDefault()
    }
    this.setState((prevState) => ({ active: !prevState.active }))
  }

  handleFile = (e) => {
    const file = e.target.files[0]
    const desc = file.name

    this.clearError()
    this.setState({ fileDescription: desc, image: file })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.clearError()
    this.setState({ isSubmitting: true })
    this.uploadFile(this.state.image, this.state.description)
    this.setState({ isSubmitting: false })
  }

  handleInputChange = (e) => {
    const target = e.target
    const name = target.name
    const value = target.value

    this.clearError()
    this.setState({
      [name]: value
    })
  }

  uploadFile = (image, description) => {
    const data = new FormData()
    data.append('description', description)
    data.append('image', image)

    axios.post('/api/posts', data)
        .then((response) => {
          this.setState({
            image: null,
            description: '',
            fileDescription: '',
            active: false,
            redirect: true,
            newPostId: response.data.id,
          })
        })
        .catch((err) => {
          console.log(err)
          const message = err.response.data.message
          let error = ''


          if (message.hasOwnProperty('image')) {
            if (!this.state.image) {
              error = 'You must select an image first.'
            } else {
              error = 'There is something wrong with the selected image.'
            }
          } else if (message.hasOwnProperty('description')) {
            error = 'You must provide a description.'
          }

          this.setState({ error })
        })
  }

  clearError = () => {
    this.setState({ error: '' })
  }

  render() {
    const state = this.state
    const classes = {
      modal: state.active ? ' is-active' : '',
      submitting: state.isSubmitting ? ' is-loading' : '',
    }
    let error = null
    let fileDescription = null

    if (state.redirect) {
      this.setState({ redirect: false })
      return <Redirect to={`/post/${this.state.newPostId}`} />
    }

    if (state.error) {
      error = (
        <article className="message is-danger">
          <div className="message-body">{state.error}</div>
        </article>
      )
    }

    if (state.fileDescription) {
      fileDescription = (
        <span className="file-name">
          {state.fileDescription}
        </span>
      )
    }

    return (
      <CSSTransitionGroup
        transitionName="modal"
        transitionEnterTimeout={150}
        transitionLeaveTimeout={100}
      >
        {state.active && (
           <div className={'modal is-active'}>
             <div className="modal-background" onClick={this.toggleActive}></div>
             <div className="modal-content">
               <div className="box">
                 {error}
                 <form onSubmit={this.handleSubmit}>
                   <div className="field">
                     <div className="file is-centered is-primary is-boxed has-name">
                       <label className="file-label">
                         <input
                           className="file-input"
                           onChange={this.handleFile}
                           name="image"
                           type="file"
                           accept=".jpg, .jpeg, .png"
                         />
                         <span className="file-cta">
                           <span className="file-icon">
                             <i className="ion-images is-size-3"></i>
                           </span>
                           <span className="file-label">
                             Select an image
                           </span>
                         </span>
                         {fileDescription}
                       </label>
                     </div>
                   </div>
                   <div className="field">
                     <div
                       className={'control' + classes.submitting}
                     >
                       <textarea
                         className="textarea"
                         name="description"
                         placeholder="Description..."
                         disabled={this.state.isSubmitting}
                         onChange={this.handleInputChange}
                         value={this.state.description}
                       />
                     </div>
                   </div>

                   <div className="field is-grouped">
                     <div className="control">
                       <input
                         type="submit"
                         className="button is-primary is-outlined"
                         value="Submit"
                       />
                     </div>
                     <div className="control">
                       <button
                         className="button is-primary is-inverted"
                         onClick={this.toggleActive}
                       >
                         Cancel
                       </button>
                     </div>
                   </div>
                 </form>
               </div>
             </div>
           </div>
        )}
      </CSSTransitionGroup>
    )
  }
}

export default UploadDialog
