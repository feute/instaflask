import React, { Component } from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { CSSTransitionGroup } from 'react-transition-group'

const Post = ({ id, description, image_url, created_at }) => (
  <CSSTransitionGroup
    transitionName="post"
    transitionAppear={true}
    transitionAppearTimeout={250}
    transitionEnter={false}
    transitionLeave={false}
    component="div"
    className="post"
  >
    <div className="card">
      <div className="card-image">
        <figure className="image is-squared">
          <img
            alt={description}
            src={image_url}
          />
        </figure>
      </div>
      <div className="card-content">
        <div className="content">
          <Link
            className="is-size-7 has-text-grey-light is-uppercase"
            to={`/post/${id}`}
          >
            {moment(created_at).fromNow()}
          </Link>
          <br />
          {description}
        </div>
      </div>
    </div>
  </CSSTransitionGroup>
)

export default Post
