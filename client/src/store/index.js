// State shape:
//
// {
//   isFetching: false,
//   posts: []
// }

import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'
import reducers from '../reducers'

const loggerMiddleware = createLogger()

const configureStore = (preloadedState) => {
  return createStore(
    reducers,
    preloadedState,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  )
}

export default configureStore
