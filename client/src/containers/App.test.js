import React from 'react'
import { shallow } from 'enzyme'

import App from './App'
import Header from '../components/Header'

it('renders without crashing', () => {
  shallow(<App />)
})

it('renders the header', () => {
  const wrapper = shallow(<App />)
  expect(wrapper.contains(<Header />)).toEqual(true)
})
